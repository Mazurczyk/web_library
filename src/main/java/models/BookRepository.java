package models;

import java.util.List;

public interface BookRepository {
    void addBook(Book book);

    List<Book> getBookList();
}
