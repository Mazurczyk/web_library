package models;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class LocalMemoryBookRepository implements BookRepository {
    private final List<Book> bookList;

    public LocalMemoryBookRepository() {
        this.bookList = new ArrayList<>();
    }

    @Override
    public void addBook(Book book) {
        bookList.add(book);
    }

    @Override
    public List<Book> getBookList() {
        return bookList;
    }
}
