package servlets;

import models.Book;
import models.BookRepository;

import javax.inject.Inject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebServlet(name = "LibraryServlet", urlPatterns = "/library")
public class LibraryServlet extends HttpServlet {

    private final static String AUTHOR = "author";
    private final static String TITLE = "title";
    private final static String ISBN = "isbn";
    private final static String BOOK_LIST = "bookList";
    private final static String LIBRARY_VIEW = "library.jsp";

    @Inject
    private BookRepository repository;

    @Inject
    private transient Logger logger;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String author = request.getParameter(AUTHOR);
        String title = request.getParameter(TITLE);
        String isbn = request.getParameter(ISBN);
        repository.addBook(new Book(author, title, isbn));
        logger.info("New record has been added with parameters: " + author + "; " + title + "; " + isbn);

        request.setAttribute(BOOK_LIST, repository.getBookList());
        RequestDispatcher view = request.getRequestDispatcher(LIBRARY_VIEW);
        view.forward(request, response);
    }
}
