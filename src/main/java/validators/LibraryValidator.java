package validators;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

import java.util.logging.Logger;
import java.util.stream.Stream;

@WebFilter(servletNames = "LibraryServlet", urlPatterns = "/library")
public class LibraryValidator implements Filter {

    private final static String AUTHOR = "author";
    private final static String VALID_LETTER = "A";
    private final static String NOT_VALID_VIEW = "newRecordNotValid.jsp";
    private final static String WARNING_MSG = "Not valid post request parameter Author";

    @Inject
    private transient Logger logger;

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String author = request.getParameter(AUTHOR);

//        boolean isValid = Stream.of(author)
//                .map(a -> a.split(" "))//
//                .filter(n -> {
//                    String forename = n[0];
//                    String surname = n[n.length - 1];
//                    return forename.startsWith(VALID_LETTER) || surname.startsWith(VALID_LETTER);
//                })
//                .anyMatch(n -> {
//                    String forename = n[0];
//                    String surname = n[n.length - 1];
//                    return forename.startsWith(VALID_LETTER) || surname.startsWith(VALID_LETTER);
//                });

        boolean isValid = Stream.of(author)
                .map(a -> a.split(" "))//
                .filter(n -> {
                    String forename = n[0];
                    String surname = n[n.length - 1];
                    return forename.startsWith(VALID_LETTER) || surname.startsWith(VALID_LETTER);
                })
                .anyMatch(n -> {
                    String forename = n[0];
                    String surname = n[n.length - 1];
                    return forename.startsWith(VALID_LETTER) || surname.startsWith(VALID_LETTER);
                });

        if (isValid) chain.doFilter(request, response);
        else valid(request, response);
    }

    @Override
    public void destroy() {

    }

    private void valid(ServletRequest request, ServletResponse response) throws ServletException, IOException {

        logger.warning(WARNING_MSG);
        RequestDispatcher view = request.getRequestDispatcher(NOT_VALID_VIEW);
        view.forward(request, response);
    }
}
