<%--
  Created by IntelliJ IDEA.
  User: rfmz
  Date: 2019-08-31
  Time: 21:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>New book</title>
</head>
<body>
<h2> Add new book</h2>
<form name="addBook" method="post" action="library">
    <fieldset>
        <legend>Book information:</legend>
        <br>
        Author:<br>
        <input type="text" name="author" value="<%out.print(request.getParameter("author"));%>"
               STYLE="border-color: red"/>
        <font color="red"> not valid! forename or surname should start from letter ‘A’ </font><br><br>
        Title:<br>
        <input type="text" name="title" value="<%out.print(request.getParameter("title"));%>"/><br><br>
        ISBN:<br>
        <input type="text" name="isbn" value="<%out.print(request.getParameter("isbn"));%>"/><br><br>
        <input type="submit" value="Add"/>
    </fieldset>
</form>
</body>
</html>
