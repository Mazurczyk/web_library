<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="models.Book" %>
<%--
  Created by IntelliJ IDEA.
  User: rfmz
  Date: 2019-08-29
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Library</title>
</head>
<body>
<h1> Welcome in web library! </h1>
<h2> List of books: </h2>
<ul>
    <%
        List bookList = (List) request.getAttribute("bookList");

        if (bookList == null || bookList.size() == 0) {
//            Stream.of(bookList)
//                    .map(b -> (Book) b)
//                    .forEach(b -> {
//                        try {
//                            out.println("<li>" +
//                                    b.getAuthor() + "; " +
//                                    b.getTitle() + "; " +
//                                    b.getIsbn() + "</li>");
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    });
            out.println("<p> No records </p>");
        } else {

            Iterator i = bookList.iterator();
            Book book;
            while (i.hasNext()) {
                book = (Book) i.next();
                out.println("<li>" + book.getAuthor() + "; " + book.getTitle() + "; " + book.getIsbn() + ";</li>");
            }
        }
    %>
</ul>
<a href="newRecord.jsp"> Add new record </a>
</body>
</html>